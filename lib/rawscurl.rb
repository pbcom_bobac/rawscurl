class AwsCurl
    require 'uri'
    require 'cgi'
    require 'digest'
    require 'openssl'
    require 'net/http'
    
    def self.url_path_to_dict(path)
        URI.parse(path)
    end
    
    def self.make_request(
        method: "GET",
        service: "execute-api",
        region:,
        uri:,
        headers: "",
        data: "",
        access_key:,
        secret_key:,
        security_token: "",
        data_binary: "",
        connection_id:
    )
        uri_dict = url_path_to_dict(uri)
        host = uri_dict.host
        query = uri_dict.query
        #canonical_uri = CGI.escape(uri_dict.path)
        canonical_uri = uri_dict.path + "/" + CGI.escape("@connections") + "/" + CGI.escape(connection_id)
        port = uri_dict.port
    
        # puts "host: #{host}"
        # puts "query: #{query}"
        # puts "canonical_uri: #{canonical_uri}"
        # puts "port: #{port}"
    
        def self.getSignatureKey key, dateStamp, regionName, serviceName
            kDate = OpenSSL::HMAC.digest('sha256', "AWS4" + key, dateStamp)
            kRegion = OpenSSL::HMAC.digest('sha256', kDate, regionName)
            kService = OpenSSL::HMAC.digest('sha256', kRegion, serviceName)
            kSigning = OpenSSL::HMAC.digest('sha256', kService, "aws4_request")
        
            kSigning
        end
    
        def self.sha256_hash(val)
            Digest::SHA256.hexdigest(val)
        end
    
        def self.sha256_hash_for_binary_data(val)
            Digest::SHA256.hexdigest(val)
        end
    
        t = Time.now.utc
        amzdate = t.strftime('%Y%m%dT%H%M%SZ')
        datestamp = t.strftime('%Y%m%d')
    
        canonical_querystring = uri_dict.normalize.query.nil? ? "" : uri_dict.normalize.query
        fullhost = port.nil? ? host + ":" + port : host
    
        canonical_headers = "host:#{fullhost}\n" +
            "x-amz-date:#{amzdate}\n"
    
        unless security_token&.empty?
            canonical_headers += "x-amz-security-token:#{security_token}\n"
        end
    
        signed_headers = "host;x-amz-date"
    
        unless security_token&.empty?
            signed_headers += ";x-amz-security-token"
        end
    
        payload_hash = data_binary&.empty? ? sha256_hash(data) : sha256_hash_for_binary_data(data)
    
        canonical_request = method + "\n" +
            canonical_uri + "\n" +
            canonical_querystring + "\n" +
            canonical_headers + "\n" +
            signed_headers + "\n" +
            payload_hash
    
        # puts "CANONICAL REQUEST = #{canonical_request}"
    
        algorithm = "AWS4-HMAC-SHA256"
        credential_scope = datestamp + "/" +
            region + "/" + 
            service + "/" +
            "aws4_request"
    
        string_to_sign = algorithm + "\n" +
            amzdate + "\n" +
            credential_scope + "\n" +
            sha256_hash(canonical_request)
    
        # puts "STRING TO SIGN = #{string_to_sign}"
    
        signing_key = getSignatureKey(secret_key, datestamp, region, service)
    
        encoded = string_to_sign.encode("utf-8")
        signature = OpenSSL::HMAC.hexdigest('sha256', signing_key, encoded)
    
        # puts "Signature: #{signature}"
    
        authorization_header = algorithm + " " +
            "Credential=" + access_key + "/" + credential_scope + ", " +
            "SignedHeaders=" + signed_headers + ", " +
            "Signature=" + signature
    
        # puts "AUTHORIZATION HEADER: #{authorization_header}"
    
        headers = {
            "Authorization": authorization_header,
            "x-amz-date": amzdate,
            #"x-amz-security-token": security_token,
            "x-amz-content-sha256": payload_hash
        }
    
        # puts "Headers: #{headers}"
    
        send_request(uri_dict, data, headers, method, connection_id)
    end
    
    def self.send_request(uri, data, headers, method, connection_id)
        # puts "************* SENDING REQUEST ***************"
        # puts "URI: #{uri}"
        # puts "DATA: #{data}"
        # puts "HEADERS: #{headers}"
        # puts "METHOD: #{method}"
    
        if method == "GET"
            req = Net::HTTP::Get.new(uri.to_s + "/@connections" + "/" + connection_id, initheader = headers)
        elsif method == "POST"
            req = Net::HTTP::Post.new(uri.to_s + "/@connections" + "/" + connection_id, initheader = headers)
            req.body = data
        elsif method == "DELETE"
            req = Net::HTTP::Delete.new(uri.to_s + "/@connections" + "/" + connection_id, initheader = headers)
        end
        https = Net::HTTP.new(uri.host, uri.port)
        https.use_ssl = true
    
        res = https.request(req)
    
        result = res.body
        status_code = res.code
        # puts "HTTP Status code: #{status_code}, (200=Connected, 204=Disconnected, 400=Invalid connectionId, 410=Gone)"
        # puts res
    
        # puts "************* REQUEST SENT ******************"
        return status_code, result
    end
    
    
end