Gem::Specification.new do |s|
    s.name        = 'rawscurl'
    s.version     = '0.0.0'
    s.date        = '2019-10-15'
    s.summary     = "Ruby AWS WebSocket API library"
    s.description = "Ruby AWS WebSocket API library. Ported from awscurl python script."
    s.authors     = ["Robert Houser"]
    s.email       = 'rob.h@altiwi.com'
    s.files       = ["lib/rawscurl.rb"]
    s.homepage    =
      'https://rubygems.org/gems/rawscurl'
    s.license       = 'MIT'
end